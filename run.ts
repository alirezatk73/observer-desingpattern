import { Sale } from "./sale";
import { Customer } from "./Customer";

// Creates a new Sale on the store
const DigiKalaSale = new Sale();

// Creates our customers
const clientA = new Customer(
  "MOHAMMAD MOEIN",
  "MOHAMMAD MOEIN@email.com",
  "Iphone X"
);
const clientB = new Customer("HESAM", "HESAM@email.com", "MacBook Pro 2020");
const clientC = new Customer("NASER", "NASER@email.com", "PS5");
const clientD = new Customer("YASAMAN", "YASAMAN@email.com", "COCO Chanel");

// Add our customers to the subscribers list
DigiKalaSale.addSubscriber(clientA);
DigiKalaSale.addSubscriber(clientB);
DigiKalaSale.addSubscriber(clientC);
DigiKalaSale.addSubscriber(clientD);

// Notify all of our clients about their
//fave product on super sale
DigiKalaSale.notifiyingSubscribers();
