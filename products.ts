export interface ISubscriber {
  sendEmail(): void;
}
