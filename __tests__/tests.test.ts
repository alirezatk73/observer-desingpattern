import { Customer } from "../customer";
import { Sale } from "../sale";

describe("Customers", () => {
  let clientA: Customer;
  beforeEach(() => {
    clientA = new Customer("Ali", "Ali@email.com", "Macbook");
  });

  it("should check if it created the correct instance of Customer", () => {
    expect(clientA).toBeInstanceOf(Customer);
  });
  it("should Send Email to Customer", () => {
    const sendEmail = (Customer.prototype.sendEmail = jest.fn());
    const customer = new Customer("guy", "smiley@email.com", "emojis");
    const expected = `Informed ${customer.name},that ${customer.email} is available`;
    customer.sendEmail();
    expect(sendEmail).toHaveBeenCalledTimes(1);
    expect(expected).toContain(customer.name);
    expect(expected).toContain(customer.email);
  });
});

describe("Sales", () => {
  let DigikalaSale: Sale;
  beforeEach(() => {
    DigikalaSale = new Sale();
  });

  it("should check if it created the correct instance of Sale", () => {
    expect(DigikalaSale).toBeInstanceOf(Sale);
  });
});
