import { ISubscriber } from "./products";

export class Customer implements ISubscriber {
  name: string;
  email: string;
  productIntrest: String;

  constructor(name: string, email: string, productIntrest: string) {
    this.name = name;
    this.email = email;
    this.productIntrest = productIntrest;
  }

  sendEmail(): void {
    console.log(
      `Informed ${this.name},that ${this.productIntrest} is available`
    );
    // Code to send a mail for the customer, informing him/her that
    //their favorite product is available
  }
}
