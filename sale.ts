import { ISubscriber } from "./products";

export class Sale {
  private subscribers: ISubscriber[];

  constructor() {
    this.subscribers = [];
  }

  addSubscriber(sub: ISubscriber) {
    this.subscribers.push(sub);
  }

  notifiyingSubscribers() {
    console.log("Notifying customers:");
    setTimeout(() => {
      this.subscribers.map((observer) => observer.sendEmail());
    }, 2000);
  }
}
